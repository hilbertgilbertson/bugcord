<?php
/**
 * BugCord
 * @file BugCord.php
 * @version 1.1
 * @author HilbertGilbertson
 * @url https://hilbertgilbertson.website
 */

class BugCord
{
    protected $config;
    public $fields, $embeds;

    public function __construct($config)
    {
        $this->config = (object)$config;
        if(!file_exists($this->config->runfile)){
            if(!touch($this->config->runfile)){
                die("Could not create initial run file. Please ensure the file '{$this->config->runfile}' exists with write permissions for the webserver user.");
            }
            $this->lastran = time() - (60 * ($this->config->runevery + 1));
        } else {
            $runfile = trim(file_get_contents($this->config->runfile));
            if(empty($runfile) || !ctype_digit($runfile)) $runfile = time() - (60 * $this->config->runevery);
            $this->lastran = $runfile;
        }
        file_put_contents($this->config->runfile, time());

        if($this->config->debug) $this->lastran = 0;

        $this->stream_context = stream_context_create(array(
            'ssl' => array(
                'verify_peer' => false
            )
        )); //this is because the intermediate certificate on the GreyTracker site is not configured!
    }

    public function webhook_post($url, $avatar, $username, $embed = null, $message = null)
    {
        if ($embed !== null || $message != null) {
            if (!empty($username))
                $data['username'] = $username;
            if (!empty($avatar))
                $data['avatar_url'] = $avatar;
            if ($message != null)
                $data['content'] = $message;
            if ($embed != null)
                $data["embeds"][] = $embed;
            $data_string = json_encode($data);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            ));
            $result = curl_exec($ch);
            $status = curl_getinfo($ch);
            if ($status['http_code'] == 204) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function rss_fetch()
    {
        $url = $this->config->BZURL . "buglist.cgi?ctype=atom&chfieldfrom=".($this->config->debug ? "24h" : "1h");
        libxml_set_streams_context($this->stream_context);
        $rss = @simplexml_load_file($url);
        $this->embeds = array();

        if(!$rss) die("Failed to retrieve feed. :(");
        if (empty($rss))
            die("Nothing to report.");

        foreach ($rss->entry as $entry) {
            $url = parse_url($entry->id, PHP_URL_QUERY);
            parse_str($url, $qs);
            $page = @simplexml_load_file($this->config->BZURL . "show_bug.cgi?ctype=xml&id=" . $qs['id']);
            if ($page) {

                /*
                 * Bug Handling
                 *
                 * TODO: $bug->version is currently 'Public' but when Nightly is out, we could have different handling for nightly bugs based on this variable.
                 *
                 * $bug->op_sys - Windows, Linux, Mac, All. Not needed.
                 * $bug->cf_gamemode Single, Multiplayer, Both; Not used currently.
                 */

                //extract and replace some of the XML attribute data into the object
                $page->bug->reporter_name = $page->bug->reporter->attributes()->name;
                $page->bug->assigned_to_name = $page->bug->assigned_to->attributes()->name;
                foreach($page->bug->long_desc as $i => $e){
                    $e->who_name = $e->who->attributes()->name;
                }

                if (isset($page->bug->attachment))
                    unset($page->bug->attachment->data); //strip attachment data
                $bug = json_decode(json_encode($page->bug));

                $update_time = strtotime($bug->delta_ts);
                if(!$update_time || $update_time <= $this->lastran) continue;
                //if the event is from before BugCord's previous run, skip it

                $bug->url = strval($entry->id);

                $description = (isset($bug->long_desc) && isset($bug->long_desc->thetext) ? $bug->long_desc : reset($bug->long_desc));
                $lastComment = (isset($bug->long_desc) && isset($bug->long_desc->thetext) ? $bug->long_desc : end($bug->long_desc));

                $embed['msg'] = null;
                $embed = array();
                $embed['title'] = "[" . $bug->bug_id . "] " . $bug->short_desc;
                $embed['url'] = $bug->url;

                $this->fields = array(
                    'status' => array(
                        'name' => "Status",
                        'value' => $bug->bug_status,
                        'inline' => true
                    ),
                    'component' => array(
                        'name' => "Component",
                        'value' => $bug->component,
                        'inline' => true
                    ),
                    'assigned_to' => array(
                        'name' => "Assigned to",
                        'value' => $bug->assigned_to_name,
                        'inline' => true
                    ),
                    'os' => array(
                        'name' => "OS",
                        'value' => $bug->rep_platform,
                        'inline' => true
                    ),
                    'priority' => array(
                        'name' => "Priority",
                        'value' => $bug->priority,
                        'inline' => true
                    ),
                    'severity' => array(
                        'name' => "Severity",
                        'value' => ucfirst($bug->bug_severity),
                        'inline' => true
                    )
                );

                $bug->changes = 0; //an int for changes that matter
                $fixed = false;
                $confirmed = false;
                $invalid = false;
                $allchanges = $this->get_changes($bug->bug_id);
                foreach ($allchanges as $blockchange){
                    if($blockchange['time'] < $this->lastran) continue;
                    /*
                     * While it might be better to reverse the array and break at this stage,
                     * we need the data in time ascending order so that the latest changes
                     * overwrite any previous change that may have occurred in the same window
                     * of time.
                     */

                    if (isset($blockchange['changed'])) {
                        foreach ($blockchange['changed'] as $change) {
                            if ($change['item'] == "Status") {
                                $this->bolden('status');
                                $bug->changes++;
                                //increase changes count
                                if($change['to'] == "CONFIRMED" && $bug->bug_status == "CONFIRMED"){
                                    $confirmed = true;
                                }
                            } elseif ($change['item'] == "Resolution") {
                                $this->fields['status']['value'] .= " **" . $change['to'] . "**";
                                //append to status field and bolden this portion
                                //we could add this as a separate field, instead

                                if($change['to'] == "FIXED" && $bug->resolution == "FIXED"){
                                    $fixed = true;
                                } elseif($change['to'] == "INVALID" && $bug->resolution == "INVALID" || $change['to'] == "WORKSFORME" && $bug->resolution == "WORKSFORME" || $change['to'] == "WONTFIX" && $bug->resolution == "WONTFIX" || $change['to'] == "DUPLICATE" && $bug->resolution == "DUPLICATE"){
                                    $invalid = true;
                                }
                                $bug->changes++;
                            } elseif ($change['item'] == "Priority") {
                                $this->bolden('priority');
                                $bug->changes++;
                            } elseif ($change['item'] == "Severity") {
                                $this->bolden('severity');
                                $bug->changes++;
                            } elseif ($change['item'] == "OS") {
                                $this->bolden('os');
                                //could switch to emojis, but then how do we make the change clear?
                                $bug->changes++;
                            } elseif ($change['item'] == "Ever confirmed") {
                                //who needs this? Deprecated in Bugzilla
                                //could be used (where 1) to indicate the first time a bug is confirmed
                                //cake & fireworks emojis? :D
                            } elseif ($change['item'] == "Summary") {
                                //the summary change will be reflected in the title
                            }
                        }
                    }
                }
                //changes via show_activity won't include comments (don't blame me!)

                $embed['fields'] = array(
                    $this->fields['status'],
                    $this->fields['component'],
                    $this->fields['assigned_to'],
                    $this->fields['priority'],
                    $this->fields['severity']
                );

                if ($bug->delta_ts == $bug->creation_ts) {
                    /*
                     * Bug created
                     */
                    if ($description && isset($description->thetext) && strlen($description->thetext) > 0) {
                        $description = $this->attach_comment($description, $bug);
                        $short_desc = (strlen($description->thetext) <= $this->config->limit_chars_desc ? $description->thetext : substr($description->thetext, 0, $this->config->limit_chars_desc) . "...");

                        $embed['fields'][] = array(
                            'name' => "Description",
                            'value' => $short_desc
                        );
                    }
                    $embed['timestamp'] = date("Y-m-d\TH:i:s\Z", strtotime($bug->creation_ts));
                    $embed['color'] = 16601684;
                    $embed['msg'] = $bug->reporter_name . " reported a bug".(!$bug->changes ? "" : " (updated)").":";
                    $this->embeds[] = $embed;
                } else {
                    /*
                     * Updated, or commented on, or both
                     */
                    $withupdate = false;
                    if($bug->changes){
                        /*
                         * Bug updated
                         */
                        $stat = "updated";
                        $embed['color'] = 16756036;

                        if($confirmed) {
                            $stat = "confirmed";
                            $embed['color'] = 12595072;
                        }
                        elseif($fixed){
                            $stat = "fixed";
                            $embed['color'] = 4521800;
                        } elseif($invalid){
                            $embed['color'] = 8947848;
                        }
                        $embed['timestamp'] = date("Y-m-d\TH:i:s\Z", strtotime($bug->delta_ts));
                        $embed['msg'] = "Bug #" . $bug->bug_id . " was $stat:";
                        $this->embeds[] = $embed;
                        $withupdate = true;
                    }

                    if ($lastComment && isset($lastComment->thetext) && strlen($lastComment->thetext) > 0 && $bug->delta_ts == $lastComment->bug_when) {
                        /*
                         * Bug commented on
                         */
                        $lastComment = $this->attach_comment($lastComment, $bug);

                        $comment_url = $bug->url . "#c" . $lastComment->comment_count;
                        //$embed['msg'] = $lastComment->who_name . " commented on [[" . $bug->bug_id . "] " . $bug->short_desc . "](" . $comment_url . "):";
                        $embed['msg'] = null;
                        //the author embed field should already make clear it's a comment
                        unset($embed['fields']); //remove previous fields as we don't want to show that data here

                        $embed['author'] = array(
                            'name' => $lastComment->who_name,
                            'url' => $comment_url
                        );

                        $lastComment->thetext = str_replace("\n", "\n", trim($lastComment->thetext));
                        $comment = (strlen($lastComment->thetext) < $this->config->limit_chars_comment ? $lastComment->thetext : substr($lastComment->thetext, 0, $this->config->limit_chars_comment) . "...");

                        $embed['description'] = $comment;
                        $embed['timestamp'] = date("Y-m-d\TH:i:s\Z", strtotime($lastComment->bug_when));
                        if($withupdate && $bug->delta_ts == $lastComment->bug_when){
                            //comment accompanies other bug update
                            //for now, just make same colour
                            //($embed['color'] still already defined)
                        } else {
                            $embed['color'] = 7122941;
                        }
                        $this->embeds[] = $embed;
                    }
                }
            }
        }
        foreach($this->embeds as $embed){
            $msg = null;
            if(isset($embed['msg'])){
                $msg = $embed['msg'];
                unset($embed['msg']);
            }
            if($this->config->debug) print_r(json_encode($embed, JSON_PRETTY_PRINT));
            $this->webhook_post($this->config->webhookURL, null, null, $embed, $msg);
        }
    }

    private function get_changes($bug_id){
        $activity = file_get_contents($this->config->BZURL."show_activity.cgi?id=$bug_id", false, $this->stream_context);
        if(!$activity) return array();
        $adom = new DOMDocument;
        $adom->loadHTML($activity);
        $adom->preserveWhiteSpace = false;
        $table = $adom->getElementById('bug_activity');
        if(!$table) return array();
        $changes = array();
        $rows = $table->getElementsByTagName('tr');
        if(count($rows) <= 1) return array(); //there should always be a tr head
        foreach($rows as $rn => $r){
            if($rn == 0) continue;

            $item = array();
            $fields = $r->getElementsByTagName('td');
            if(count($fields) == 5){
                $item['time'] = strtotime($fields->item(1)->nodeValue);
                $item['changed'][] = array('item' => trim($fields->item(2)->nodeValue), 'from' => trim($fields->item(3)->nodeValue), 'to' => trim($fields->item(4)->nodeValue));
            } elseif(count($fields) == 3){
                $changes[(count($changes) -1)]['changed'][] = array('item' => trim($fields->item(0)->nodeValue), 'from' => trim($fields->item(1)->nodeValue), 'to' => trim($fields->item(2)->nodeValue));

            }
            if(!empty($item)) $changes[] = $item;
        }
        return $changes;
    }

    private function bolden($field){
        if(!isset($this->fields[$field])) return false;
        return $this->fields[$field]['value'] = "**".str_replace('*', '', $this->fields[$field]['value'])."**";
    }

    private function attach_comment($comment, $bug){
        if(isset($comment->attachid) && isset($bug->attachment)){
            $lastAtt = (isset($bug->attachment->attachid) ? $bug->attachment : end($bug->attachment));
            if($lastAtt->attachid == $comment->attachid){
                $lines = explode("\n", $comment->thetext);
                unset($lines[0]); //remove the line beginning 'Created attachment #n';
                if($lines[1] == $lastAtt->desc) unset($lines[1]);
                $comment->thetext = implode("\n", $lines)."\n\nUploaded [".$lastAtt->filename."](".$this->config->BZURL."attachment.cgi?id=".$lastAtt->attachid.")";
            }
        }
        return $comment;
    }
}