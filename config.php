<?php
/**
 * BugCord
 * @file config.php
 * @version 1.0
 * @author HilbertGilbertson
 * @url https://hilbertgilbertson.website
 */

$config['BZURL'] = "https://mysite.com/bugzilla/"; //the root URL of your BugZilla install (with trailing slash!)
$config['webhookURL'] = "https://discordapp.com/api/webhooks/..."; //the URL of your webhook

$config['runfile'] = "/path/to/bzrun"; //path to an empty text file used to store the timestamp of last execution. www-data/apache must have write access to this file
$config['runevery'] = 5; //how frequently the script runs (in minutes); set up a cron to match this frequency

$config['limit_chars_comment'] = 250; //max length of new comments to post; Cannot exceed 2000
$config['limit_chars_desc'] = 250; //max length of bug description to post (new bugs only); Cannot exceed 1024

$config['debug'] = false; //Don't enable unless you're prepared for a load of data to flood into your webhook channel. For debugging only, hence the name ;-)