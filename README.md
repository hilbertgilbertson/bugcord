# BugCord v1.1

BugCord is a PHP script for posting BugZilla updates to a Discord webhook.

This is a basic implementation, a shortcut around comprehensive editing of BugZilla. BugCord creates basic Discord webhook messages for issue updates in BugZilla, based on native RSS and XML data.

BugCord does not post to multiple channels, and can only distinguish three categories of issue update: new bug reports; a comment on a bug report (which may coincide with other changes which won't be noticed) (`hilbert commented on [#1] Bug title`), and everything else (`Bug #1 was updated`).

## Requirements
* PHP 7.2+

## Installation

### Upload files

Upload config.php, BugCord.php and run.php to the same directory on your server. This could be a publicly accessible directory if you wish for the script to be able to be called externally; otherwise a directory above webroot is recommended.

### Configuration

Configure your settings in config.php, following the instructions within.

### Trial run

You can check if everything works by setting $config['debug'] in config.php to true and executing run.php. Warning: this may result in posting updates for all bugs that changed in the last 24 hours! Presuming everything appears to work, don't forget to change $config['debug'] back to false.

### Cron

Configure a cron to execute run.php e.g. `*/5 * * * * /usr/bin/php /path/to/bugcord/run.php >/dev/null 2>&1` or `*/5 * * * * wget -O - http://mysite.com/bugcord/run.php >/dev/null 2>&1` if BugCord is placed within webroot.

Note about access control: if you have uploaded the BugCord directory within your webroot, it is recommended you use access control (such as with .htaccess) to limit access to specified IPs. You don't want strangers executing your cron.

## License

Released under MIT License

Copyright (c) 2018 HilbertGilbertson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
