# ChangeLog

## v1.1

### Features
* Comments no longer have the *username commented on bug...* title, as it's apparent from the author field in the message
* Changes are now shown in bold
* Additional recognition of particular status changes (such as *resolved fixed*, which now appears green)

### Bug Fixes
* Attachments now show on bug report messages as well as comment messages
* Comments are now posted *in addition* to any other changes, rather than instead of them
* Fixed an issue with username attributes