<?php
/**
 * BugCord
 * @file run.php
 * @version 1.0
 * @author HilbertGilbertson
 * @url https://hilbertgilbertson.website
 */
require 'config.php';
require 'BugCord.php';

$bc = new BugCord($config);

date_default_timezone_set('UTC'); //should match the timezone of the remote server; leave as UTC unless you know better
$bc->rss_fetch();
die("BugCord Cron Executed");